import React, { Component } from "react";
import { connect } from 'react-firebase';


class CharacterLogin extends Component {
  constructor(props) {
    super(props);
    let hasCookie = false;

    if (this.getCookie("characterId")){
      hasCookie = true;
    }

    this.state = {
      characterId: this.getCookie("characterId"),
      hasCookie: hasCookie,
    }

    this.submitHandler = this.submitHandler.bind(this);
    this.createCharacter = this.createCharacter.bind(this);
    this.editCharacter = this.editCharacter.bind(this);
  }

  getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
      var c = ca[i];
      while (c.charAt(0) === ' ') {
          c = c.substring(1);
      }
      if (c.indexOf(name) === 0) {
          return c.substring(name.length, c.length);
      }
    }
    return "";
  }

  setCookie(cvalue) {
    var d = new Date();
    d.setTime(d.getTime() + (30*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = "characterId=" + cvalue + ";" + expires + ";path=/";
  }

  submitHandler(event) {
    event.preventDefault();
    this.setCookie(this.state.characterId);
    this.props.loginWithCharacterId(this.state.characterId);
  }

  createCharacter() {
    const defaultCharacter = this.props.firebaseDefaultCharacter;
    const characterId = this.props.firebaseCreateCharacter(defaultCharacter).key;
    this.setCookie(characterId);
    this.props.editWithCharacterId(characterId);
  }

  editCharacter() {
    const characterId = this.state.characterId;
    this.setCookie(characterId);
    this.props.editWithCharacterId(characterId);
  }

  render() {
    return (
      <div className="character-login">
        <div className="row">
          <div className="col-sm-6 col-sm-offset-3">
            { this.state.hasCookie &&
              <button type="button" onClick={this.submitHandler} className="btn btn-success btn-lg btn-block">log in as {this.state.characterId}</button>
            }
            <button type="button" onClick={this.createCharacter} className="btn btn-default btn-lg btn-block">create a new character</button>
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">welcome v0.1.0</h3>
              </div>
              <div className="panel-body">
                <form onSubmit={this.submitHandler} >
                  
                  <input type="text" className="form-control" value={this.state.characterId} onChange={(event)=>{this.setState({characterId:event.target.value})}} required/>

                  <div className="text-center">
                    <div className="btn-group" role="group">
                      <button type="submit" className="btn btn-default btn-lg">Log in</button>
                      <button type="button" onClick={this.editCharacter} className="btn btn-default btn-lg">Edit character</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )        
  }
}

CharacterLogin.propTypes = {
  editWithCharacterId: React.PropTypes.func.isRequired,
  loginWithCharacterId: React.PropTypes.func.isRequired,
}

export default connect((props, ref) => ({
  firebaseDefaultCharacter: '/defaultCharacter',
  firebaseCreateCharacter: (value) => ref("/characters").push(value),
}))(CharacterLogin)