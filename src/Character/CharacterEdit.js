import React, { Component } from "react";
import { connect } from 'react-firebase';

class CharacterEdit extends Component {
  constructor(props) {
    super(props);

     this.state = {
      name: "",
    }

    this.save = this.save.bind(this);
    this.cancel = this.cancel.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    this.setState({
      name: nextProps.firebaseCharacter.name
    })
  }

  save(event) {
    event.preventDefault();
    this.props.firebaseSaveCharacterName(this.state.name)
    this.props.loginWithCharacterId(this.props.characterId);
  }

  cancel() {
    this.props.loginWithCharacterId(this.props.characterId);
  }

  render() {
    // if(!this.props.firebaseCharacter) {
    //   return null
    // }

    return (
      <div className="character-edit">
        <div className="row">
          <div className="col-sm-6 col-sm-offset-3">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title">Character id: {this.props.characterId}</h3>
              </div>
              <div className="panel-body">
                <form onSubmit={this.save} >                  
                  <div className="form-group">
                    <label>Name:</label>
                    <input
                      type="text"
                      className="form-control"
                      value={this.state.name}
                      onChange={(event)=>{this.setState({name:event.target.value})}}
                      required/>
                  </div>
                  <div className="text-center">
                    <div className="btn-group" role="group">
                      <button type="submit" className="btn btn-default btn-lg">save</button>
                      <button type="button" onClick={this.cancel} className="btn btn-default btn-lg">cancel</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
      </div>
    )        
  }
}

CharacterEdit.propTypes = {
  loginWithCharacterId: React.PropTypes.func.isRequired,
  characterId: React.PropTypes.string.isRequired,
}

export default connect((props, ref) => ({
  firebaseCharacter: '/characters/'+props.characterId,
  firebaseSaveCharacterName: (value) => ref("/characters/"+props.characterId+"/name").set(value),
}))(CharacterEdit)