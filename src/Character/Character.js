import React, { Component } from "react";
import CharacterEdit from "./CharacterEdit";
import CharacterLogin from "./CharacterLogin";

class Character extends Component {
  constructor(props) {
    super(props);

    this.state = {
      characterId: "",
      edit: false,
    }
  }

  render() {
    return (
      <div className="character">        
        { this.state.edit ?
          <CharacterEdit 
            characterId={this.state.characterId}
            loginWithCharacterId={this.props.saveCharacterIdtoApp}
          />
          :
          <CharacterLogin
            editWithCharacterId={(id)=>this.setState({
              characterId:id,
              edit: true
            })}
            loginWithCharacterId={this.props.saveCharacterIdtoApp}
          />          
        }
      </div>
    )        
  }
}

Character.propTypes = {
  saveCharacterIdtoApp: React.PropTypes.func.isRequired,
}

export default Character