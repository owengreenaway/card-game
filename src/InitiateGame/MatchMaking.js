import React, { Component } from "react";
import { connect } from 'react-firebase';

class MatchMaking extends Component {
  constructor(props) {
    super(props);
    this.hostGame = this.hostGame.bind(this);
    this.joinGame = this.joinGame.bind(this);
    this.singlePlayer = this.singlePlayer.bind(this);
  }

  hostGame(){
    // Create game
    const defaultGame = this.props.firebaseDefaultGame;
    const gameId = this.props.firebaseCreateGame(defaultGame).key;

    // Add first player to game
    const character = this.props.firebaseCharacter;
    const deck = this.drawCards(character.deck).deck;
    const hand = this.drawCards(character.deck).hand;
    this.props.firebaseSave("/games/"+gameId+"/player/0/character/",character.name);
    this.props.firebaseSave("/games/"+gameId+"/player/0/deck/",deck);
    this.props.firebaseSave("/games/"+gameId+"/player/0/hand/",hand);

    // Add game to matchMaking
    this.props.firebaseSave("/matchMaking/"+gameId,{
      name: character.name
    })

    // Update App state
    this.props.saveToApp({
      playerId: 0,
      opponentId: 1,
    });

    // Wait for player two
    this.props.saveGameId(gameId);
  }

  joinGame(gameId){
    // Add second player to game
    const character = this.props.firebaseCharacter;
    const deck = this.drawCards(character.deck).deck;
    const hand = this.drawCards(character.deck).hand;
    this.props.firebaseSave("/games/"+gameId+"/player/1/character/",character.name);
    this.props.firebaseSave("/games/"+gameId+"/player/1/deck/",deck);
    this.props.firebaseSave("/games/"+gameId+"/player/1/hand/",hand);

    // Delete matchMaking game
    this.props.firebaseSave("/matchMaking/"+gameId,null);
    
    // Tell other player we have joined
    this.props.firebaseSave("/games/"+gameId+"/gameStats/hasTwoPlayers",true);

    // Update App state
    this.props.saveToApp({
      gameId: gameId,
      playerId: 1,
      opponentId: 0,
    });
  }

  singlePlayer() {
    console.log("Single player");
    const character = this.props.firebaseCharacter;
    const deck = this.drawCards(character.deck).deck;
    const hand = this.drawCards(character.deck).hand;    

    // Create game
    const defaultGame = this.props.firebaseDefaultGame;
    const gameId = this.props.firebaseCreateGame(defaultGame).key;
    
    // Add first player to game    
    this.props.firebaseSave("/games/"+gameId+"/player/0/character/",character.name);
    this.props.firebaseSave("/games/"+gameId+"/player/0/deck/",deck);
    this.props.firebaseSave("/games/"+gameId+"/player/0/hand/",hand);

    // Add Jarvis
    const jarvis = this.props.firebaseCharacter.jarvis;
    const jarvisDeck = this.drawCards(jarvis.deck).deck;
    const jarvisHand = this.drawCards(jarvis.deck).hand;
    this.props.firebaseSave("/games/"+gameId+"/player/1/character/",jarvis.name);
    this.props.firebaseSave("/games/"+gameId+"/player/1/deck/",jarvisDeck);
    this.props.firebaseSave("/games/"+gameId+"/player/1/hand/",jarvisHand);

    // Update App state
    this.props.saveToApp({
      gameId: gameId,
      playerId: 0,
      opponentId: 1,
      singlePlayer: true,
    });

    // Save to game
    this.props.firebaseSave("/games/"+gameId+"/gameStats/hasTwoPlayers",true);
  }

  drawCards(deck) {
    let hand = [];

    for (let i = 0; i < 10; i++) {
      let playerRandomCard = deck[Math.floor(Math.random() * deck.length)];
      hand = this.addToArray(playerRandomCard, hand);
      deck = this.deleteFromArray(playerRandomCard, deck);
    }

    return {
      deck: deck,
      hand: hand,
    }
  }

  deleteFromArray(cardObj, array) {
    if(typeof array === "undefined") {
      return []
    }
    const arrayClone = array.slice(0);
    return arrayClone.filter(function (el) { return el.id !== cardObj.id; });
  }

  addToArray(cardObj, array) {
    if(typeof array === "undefined") {
      return [cardObj]
    }
    const arrayClone2 = array.slice(0);
    arrayClone2.push(cardObj);
    return arrayClone2;
  }

  render() {
    const games = this.props.firebaseMatchMaking;
    let gamesArray = [];

    if(games){
      Object.entries(games).forEach(
          ([key, game]) => {
            gamesArray.push(
              <tr key={key}>
                <td >
                  <button onClick={()=>{this.joinGame(key)}} className="btn btn-default btn-lg btn-block">
                    play against {game.name}
                  </button>
                </td>
              </tr>)
          }
      );
    }
    
    return (
      <div className="match-making">
        <div className="row">
          <div className="col-sm-6 col-sm-offset-3">
            <div className="panel panel-default">

              <div className="panel-heading text-center">
                <h3 className="panel-title">{this.props.firebaseCharacter && this.props.firebaseCharacter.name}</h3>
              </div>

              <div className="panel-body">
                <div className="text-center">
                    <div className="btn-group" role="group">
                      <button type="button" onClick={this.hostGame} className="btn btn-default btn-lg">host</button>
                      <button type="button" onClick={this.singlePlayer} className="btn btn-default btn-lg">single player</button>
                    </div>
                  </div>
              </div>

            </div>
          
            { gamesArray.length > 0 &&
              <div className="panel panel-default">
                <div className="panel-heading text-center">
                  <h3 className="panel-title">join</h3>
                </div>
                <table className="table text-center">
                  <tbody>
                    {gamesArray}
                  </tbody>
                </table>
              </div> 
            }
          </div>
        </div>
      </div>
    );
  }
}

MatchMaking.PropTypes = {
  characterId: React.PropTypes.string.isRequired,
  saveToApp: React.PropTypes.func.isRequired,
  saveGameId: React.PropTypes.func.isRequired,
}

// MatchMaking.defaultProps = {
//   matches: {},
// }

export default connect((props, ref) => ({
  firebaseMatchMaking: '/matchMaking',
  firebaseDefaultGame: '/defaultGame',
  firebaseCharacter: '/characters/'+props.characterId,
  firebaseCreateGame: (value) => ref("/games").push(value),
  firebaseSave: (url,value) => ref(url).set(value),
}))(MatchMaking)
