import React, { Component } from "react";
import MatchMaking from './MatchMaking';
import TwoPlayers from './TwoPlayers';

class InitiateGame extends Component {
  constructor(props) {
    super(props);

    this.state = {
        gameId: null,
    }

    // this.host = this.host.bind(this);
  }

  render() {
    return (      
      <div className="initiate-game">
        { this.state.gameId ?
          <TwoPlayers
            gameId={this.state.gameId}
            saveToApp={this.props.saveToApp}
          />
          :
          <MatchMaking
            characterId={this.props.characterId}
            saveGameId={(value)=>{ this.setState({gameId:value}) } }
            saveToApp={this.props.saveToApp}
          />
        }
      </div>
    );
  }
}

InitiateGame.PropTypes = {
  characterId: React.PropTypes.string.isRequired,
  saveToApp: React.PropTypes.func.isRequired,
  singlePlayer: React.PropTypes.bool.isRequired,
}

export default InitiateGame
