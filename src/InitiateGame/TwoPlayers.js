import React, { Component } from "react";
import { connect } from 'react-firebase';

class TwoPlayers extends Component {
  
  render() {
    if(this.props.firebaseHasTwoPlayers){
      this.props.saveToApp({gameId:this.props.gameId});
    }
    
    return (
      <div className="two-players">
        <div className="row">
          <div className="col-sm-6 col-sm-offset-3">
            <div className="panel panel-default">
              <div className="panel-heading">
                <h3 className="panel-title text-center">waiting</h3>
              </div>
              <div className="panel-body">
                Waiting for another player to join the game...
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

TwoPlayers.PropTypes = {
  gameId: React.PropTypes.string.isRequired,
  saveToApp: React.PropTypes.func.isRequired,
}

export default connect((props, ref) => ({
  firebaseHasTwoPlayers: "/games/"+props.gameId+"/gameStats/hasTwoPlayers",
}))(TwoPlayers)
