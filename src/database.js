import firebase from 'firebase';

const config = {
  apiKey: "AIzaSyAyW3Fm2sD2LDjkFMbpL5_UqrNhS_IERg8",
  authDomain: "card-game-fc259.firebaseapp.com",
  databaseURL: "https://card-game-fc259.firebaseio.com",
  projectId: "card-game-fc259",
  storageBucket: "card-game-fc259.appspot.com",
  messagingSenderId: "170521616070"
};

firebase.initializeApp(config);
const database = firebase.database();

export default database;