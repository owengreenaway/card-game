import React, { Component } from 'react';

class Jarvis extends Component {
  constructor(props) {
    super(props);
    this.state = {
      thinking: false,
    };

    this.chooseCard = this.chooseCard.bind(this);
    this.playCard = this.playCard.bind(this);
    this.think = this.think.bind(this);
    this.speak("Initializing...");
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.currentPlayer === 1 && !this.state.thinking) {
      this.setState({thinking:true})
      setTimeout(()=>{this.think(nextProps)}, 750);      
    }
  }

  speak(value){
    console.log('%cJarvis: '+value, 'background: #222; color: #bada55');
  }

  think(nextProps){
    if(!nextProps.roundEnded) {
      this.playCard(this.chooseCard());
    }

    if (!nextProps.playerPassed) {
      nextProps.saveStatsCurrentPlayer(0);
    }
    this.setState({thinking: false})
  }

  chooseCard() {
    const { jarvisHand, playerPassed, saveJarvisPassed } = this.props;

    if(playerPassed) {
      // If they pass, also pass
      saveJarvisPassed(true);
      this.speak("Passing");
      return false
    }

    if(jarvisHand.length === 0){
      saveJarvisPassed(true);
      this.speak("No cards to play. Instead passing");
      return false
    }
    
    const card = jarvisHand[Math.floor(Math.random() * jarvisHand.length)];
    this.speak( card.name+", I choose you!");
    return card
  }

  playCard(card) {
    // if no card needs to be played
    if(!card) {
      return
    }
    const { jarvisHand, jarvisBoard0, jarvisBoard1, jarvisBoard2 } = this.props;
    this.props.saveJarvisHand(this.deleteFromArray(card, jarvisHand));
    
    if (card.type === "normal") {
      if(card.row === 0) {
        this.props.saveJarvisBoard0(this.addToArray(card, jarvisBoard0));
      }
      
      if(card.row === 1) {
        this.props.saveJarvisBoard1(this.addToArray(card, jarvisBoard1));
      }

      if(card.row === 2) {
        this.props.saveJarvisBoard2(this.addToArray(card, jarvisBoard2));
      }
    }

    if (card.type === "special") {
      // Either add effect to a row OR clear all rows   
      let newSpecials = this.props.specials.slice();

      if(card.row !== -1) {
        newSpecials[card.row] = true;
      }
      else {
        newSpecials = [false, false, false];
      }
      this.props.saveStatsSpecials(newSpecials);
    }
  }

  deleteFromArray(cardObj, array) {
    if(typeof array === "undefined") {
      return []
    }
    const arrayClone = array.slice(0);
    return arrayClone.filter(function (el) { return el.id !== cardObj.id; });
  }

  addToArray(cardObj, array) {
    if(typeof array === "undefined") {
      return [cardObj]
    }
    const arrayClone2 = array.slice(0);
    arrayClone2.push(cardObj);
    return arrayClone2;
  }

  render() {
    return (
      null
    )
  }
}

Jarvis.propTypes = {
  currentPlayer: React.PropTypes.number.isRequired,
  specials: React.PropTypes.array.isRequired,
  jarvisHand: React.PropTypes.array,
  jarvisBoard0: React.PropTypes.array,
  jarvisBoard1: React.PropTypes.array,
  jarvisBoard2: React.PropTypes.array,
  playerPassed: React.PropTypes.bool.isRequired,
  roundEnded: React.PropTypes.bool.isRequired,
};

Jarvis.defaultProps = {
  jarvisHand: [],
  jarvisBoard0: [],
  jarvisBoard1: [],
  jarvisBoard2: [],
};

export default Jarvis