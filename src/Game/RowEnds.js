import React, { Component } from "react";
import Card from "./Card";

class RowEnds extends Component {
  //   constructor(props) {
  //     super(props);
  //     this.state = this.props;    
  //     this.clickHandler = this.clickHandler.bind(this);
  //   }

  render() {
    const { rowEnds, _selectCard, reverse } = this.props;
    const rowEndsClone = rowEnds.slice("");
    let rows;

    // Reverse the rows in the opponents board
    if (reverse) {
      rows = rowEndsClone.reverse();
    }
    else {
      rows = rowEndsClone;
    }

    return (
      <div className="RowEnds col-xs-2">
        <div className="row">
          <div className="col-xs-12 cardHeight border RowEnds__row">
            {
              (
                rows[0] && <Card details={rows[0]} __selectCard={_selectCard} playableCards={false} />
              )
            }
          </div>
          <div className="col-xs-12 cardHeight border RowEnds__row">
            {
              (
                rows[1] && <Card details={rows[1]} __selectCard={_selectCard} playableCards={false} />
              )
            }
          </div>
          <div className="col-xs-12 cardHeight border RowEnds__row">
            {
              (
                rows[2] && <Card details={rows[2]} __selectCard={_selectCard} playableCards={false} />
              )
            }
          </div>
        </div>
      </div>
    )
  }
}

RowEnds.propTypes = {
  _selectCard: React.PropTypes.func.isRequired,
  playableCards: React.PropTypes.bool.isRequired,
  reverse: React.PropTypes.bool,
  rowEnds: React.PropTypes.array.isRequired,
};

RowEnds.defaultProps = {
  reverse: false,
};

export default RowEnds