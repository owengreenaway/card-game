import React, { Component } from "react";

class PlayerStats extends Component {
  render() {
    const { total, passed, characterName, currentlyPlaying, showPass } = this.props;
    return (
      <div className="col-xs-12 Stats__player cardHeight">

        <div className="row">
          <div className="col-xs-12">
            <div className={"panel panel-default " + (currentlyPlaying && "panel-success") }>
              <div className="panel-heading">
                <h3 className="panel-title">{characterName}</h3>
              </div>
              { passed &&
                <div className="panel-body">
                  <div className="alert alert-danger">passed</div>
                </div>
              }
              <table className="table">
                <tbody>
                  {/*<tr>
                    <td>hand:</td>
                  </tr>*/}
                  <tr>
                    <td>score: {total[0] + total[1] + total[2]}</td>
                  </tr>
                  { !passed && showPass &&
                    <tr>
                      <td>
                        <button className="btn btn-default" onClick={this.props.pass} disabled={!currentlyPlaying}>pass</button>
                      </td>
                    </tr>
                  }
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

PlayerStats.propTypes = {
  total: React.PropTypes.array.isRequired,
};

export default PlayerStats