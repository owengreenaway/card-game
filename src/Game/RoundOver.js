import React, { Component } from "react";

class RoundOver extends Component {
  constructor(props) {
    super(props);
    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler() {
    this.props.nextRound();
  }

  render() {
    const { rounds, round, playerCharacterName, playerId, opponentCharacterName, opponentId } = this.props;

    return (
      <div className="RoundOver">
        <div className="row">
          <div className="col-sm-6 col-sm-offset-3">
            <div className="panel panel-default text-center">
              <div className="panel-heading">
              {(round + 1 < rounds.length) ?  "round over" : "game over" }
              </div>
              <table className="table">
                <tbody>
                  <tr><td>{playerCharacterName} vs {opponentCharacterName}</td></tr>
                  <tr><td>{rounds[0][playerId]} vs {rounds[0][opponentId]}</td></tr>
                  <tr><td>{rounds[1][playerId]} vs {rounds[1][opponentId]}</td></tr>
                  <tr><td>{rounds[2][playerId]} vs {rounds[2][opponentId]}</td></tr>
                </tbody>
              </table>
              { (round + 1 < rounds.length) && 
                <div className="panel-body">
                  <button onClick={this.clickHandler} className="btn btn-default">play next round</button>
                </div>
              }
            </div>
          </div>
        </div>
      </div>
    )
  }
}

// Game.propTypes = {
//   showItem: React.PropTypes.bool.isRequired,
//   heading: React.PropTypes.string.isRequired
// };

// Game.defaultProps = {
//   heading: "Heading not supplied"
// };

export default RoundOver