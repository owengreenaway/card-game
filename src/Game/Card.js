import React, { Component } from 'react';

class Card extends Component {
  constructor(props) {
    super(props);
    this.clickHandler = this.clickHandler.bind(this);
  }

  clickHandler() {
    this.props.__selectCard(this.props.details, this.props.playableCards, this.props.player);
  }

  render() {
    const { name, image } = this.props.details;
    // row, id, name, value, type, ability, class
    const imageUrl = "images/cards/" + image + "-gwent-card.jpg";
    return (
      <div className="Card" onClick={this.clickHandler}>
        <img className="Card__image" src={imageUrl} alt={name} />
        {image ? null : <div className="Card__details">
          <div>{name}</div>
        </div>}
      </div>
    )
  }
}

// Game.propTypes = {
//   showItem: React.PropTypes.bool.isRequired,
//   heading: React.PropTypes.string.isRequired
// };

// Game.defaultProps = {
//   heading: "Heading not supplied"
// };

export default Card