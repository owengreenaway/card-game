import React, { Component } from "react";

class Totals extends Component {
  //   constructor(props) {
  //     super(props);
  //     this.state = this.props;    
  //     this.clickHandler = this.clickHandler.bind(this);
  //   }

  render() {
    const { total, reverse } = this.props;

    return (
      <div className="col-xs-1 Totals">
        { reverse ?
          <div className="row">
            <div className="col-xs-12 cardHeight border background Total__row">{total[2]}</div>
            <div className="col-xs-12 cardHeight border background Total__row">{total[1]}</div>
            <div className="col-xs-12 cardHeight border background Total__row">{total[0]}</div>
          </div> :
          <div className="row">
            <div className="col-xs-12 cardHeight border background Total__row">{total[0]}</div>
            <div className="col-xs-12 cardHeight border background Total__row">{total[1]}</div>
            <div className="col-xs-12 cardHeight border background Total__row">{total[2]}</div>
          </div> }
      </div>
    )
  }
}

Totals.propTypes = {
  reverse: React.PropTypes.bool,
};

Totals.defaultProps = {
  reverse: false
};

export default Totals