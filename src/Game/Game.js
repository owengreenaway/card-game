import React, { Component } from 'react';
import Board from "./Board";
import CardInfo from "./CardInfo";
import Hand from "./Hand";
import Leader from "./Leader";
import PlayerStats from "./PlayerStats";
import RoundOver from "./RoundOver";
import RowEnds from "./RowEnds";
import Totals from "./Totals";

class Game extends Component {
  constructor(props) {
    super(props);

    this.state = {
      isSelectedPlayable: false,
      selectedCard: null,
    }

    this.selectCard = this.selectCard.bind(this);
    this.playCardHandler = this.playCardHandler.bind(this);
  }

  selectCard(card, playable) {
    // Only play your card if it's your turn
    if (playable) {
      playable = (this.props.currentPlayer === this.props.playerId);
    }

    this.setState({
      selectedCard: card,
      isSelectedPlayable: playable,
    })
  }

  playCardHandler(){
    this.setState({selectedCard: null,}) // Clear selectedCard using state
    this.props.playCard(this.state.selectedCard)
  }

  render() {
    if (this.props.roundEnded) {
      return (
        <RoundOver
          nextRound={this.props.nextRound}
          round={this.props.round}
          rounds={this.props.rounds}
          playerCharacterName={this.props.playerCharacterName}
          opponentCharacterName={this.props.opponentCharacterName}
          playerId={this.props.playerId}
          opponentId={this.props.opponentId}
        />
      )
    }

    return (
      <div className="game row">        
        <div className="col-xs-2 FirstCol">
          <div className="row">
            <div className=" Stats">
              <PlayerStats 
                hand={this.props.opponentHand}
                total={this.props.opponentTotal}
                passed={this.props.opponentPassed}
                characterName={this.props.opponentCharacterName}
                showPass={false}
                pass={this.props.pass}
                currentlyPlaying={this.props.currentPlayer === this.props.opponentId}
              />
              <div className="col-xs-12 cardHeight" />
              <PlayerStats 
                hand={this.props.playerHand}
                total={this.props.playerTotal}
                passed={this.props.playerPassed}
                characterName={this.props.playerCharacterName}
                showPass={true}
                pass={this.props.pass}
                currentlyPlaying={this.props.currentPlayer === this.props.playerId}
              />
            </div>
            <Leader card={this.props.playerLeader} />
          </div>
        </div>
        <div className="col-xs-7 SecondCol">
          <div className="row">
            <Totals total={this.props.opponentTotal} reverse={true} />
            <RowEnds
              rowEnds={this.props.opponentRowEnds}
              playableCards={false}
              _selectCard={this.selectCard}
              reverse={true}
            />
            <Board
              // TODO: pass in to board reversed instead of reverse prop
              board0={this.props.opponentBoard0}
              board1={this.props.opponentBoard1}
              board2={this.props.opponentBoard2}
              specials={this.props.specials}
              _selectCard={this.selectCard}
              reverse={true}
            />
            <Totals total={this.props.playerTotal} />
            <RowEnds
              rowEnds={this.props.playerRowEnds}
              playableCards={false}
              _selectCard={this.selectCard}
              reverse={false}
            />
            <Board
              // TODO: pass in to board reversed instead of reverse prop
              board0={this.props.playerBoard0}
              board1={this.props.playerBoard1}
              board2={this.props.playerBoard2}
              specials={this.props.specials}
              _selectCard={this.selectCard}
              reverse={false}
            />
            <Hand
              cards={this.props.playerHand}
              _selectCard={this.selectCard}
            />
          </div>
        </div>

        <div className="col-xs-3 ThirdCol">
          <div className="row Right">
            <div className="col-xs-12 cardHeight DiscardPile"></div>
            <CardInfo
              isSelectedPlayable={this.state.isSelectedPlayable}
              selectedCard={this.state.selectedCard}
              playableCards={true}
              _playCard={this.playCardHandler}
              />
            <div className="col-xs-12 cardHeight DiscardPile"></div>
          </div>
        </div>
      </div>
    )
  }
}

Game.propTypes = {
  playerId: React.PropTypes.number.isRequired,
  opponentId: React.PropTypes.number.isRequired,
  round: React.PropTypes.number.isRequired,
  rounds: React.PropTypes.array.isRequired,
  roundEnded: React.PropTypes.bool.isRequired,
  currentPlayer: React.PropTypes.number.isRequired,
  specials: React.PropTypes.array.isRequired,

  playerHand: React.PropTypes.array.isRequired,
  playerBoard0: React.PropTypes.array.isRequired,
  playerBoard1: React.PropTypes.array.isRequired,
  playerBoard2: React.PropTypes.array.isRequired,
  playerRowEnds: React.PropTypes.array.isRequired,
  playerDiscarded: React.PropTypes.array.isRequired,
  playerLeader: React.PropTypes.object.isRequired,
  playerDeck: React.PropTypes.array.isRequired,
  playerPassed: React.PropTypes.bool.isRequired,
  playerTotal: React.PropTypes.array.isRequired,

  opponentBoard0: React.PropTypes.array.isRequired,
  opponentBoard1: React.PropTypes.array.isRequired,
  opponentBoard2: React.PropTypes.array.isRequired,
  opponentRowEnds: React.PropTypes.array.isRequired,
  opponentPassed: React.PropTypes.bool.isRequired,
  opponentTotal: React.PropTypes.array.isRequired,
}

export default Game