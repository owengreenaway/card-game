import React, { Component } from 'react';
import Game from "./Game.js";
import Jarvis from "../Jarvis/Jarvis.js";

class GameLogic extends Component {
  constructor(props) {
    super(props);

    this.state = {
      playerTotal: [0,0,0],
      opponentTotal: [0,0,0],
    }

    this.playCard = this.playCard.bind(this);
    this.pass = this.pass.bind(this);
    this.nextRound = this.nextRound.bind(this);
    this.addCardToPlayerBoard = this.addCardToPlayerBoard.bind(this);
  }

  componentWillReceiveProps(nextProps) {
    // If new props comes from Firebase
    const {playerHand, playerPassed, opponentHand, opponentPassed, rounds, round} = nextProps;
    // Check if round has ended
    const playerTotal = this.calculateScore(nextProps.playerBoard0, nextProps.playerBoard1, nextProps.playerBoard2, nextProps.specials);
    const opponentTotal = this.calculateScore(nextProps.opponentBoard0, nextProps.opponentBoard1, nextProps.opponentBoard2, nextProps.specials);
    const playerSum = playerTotal[0] + playerTotal[1] + playerTotal[2];
    const opponentSum = opponentTotal[0] + opponentTotal[1] + opponentTotal[2];    
    const playerFinished = playerHand.length === 0 || playerPassed;
    const opponentFinished = opponentHand.length === 0 || opponentPassed;

    if (playerFinished && opponentFinished) {      
      this.props.savePlayerPassed(false);
      this.props.saveOpponentPassed(false);
      this.props.saveStatsRounds(this.updateRounds(rounds, round, playerSum, opponentSum, this.props.playerId, this.props.opponentId));
      this.props.saveStatsRoundEnded(true);
    }

    this.setState({
      playerTotal: playerTotal,
      opponentTotal: opponentTotal,
    });
  }

  updateRounds(rounds,round,playerSum,opponentSum,playerId,opponentId){
    let newRounds = rounds;
    newRounds[round][playerId] = playerSum;
    newRounds[round][opponentId] = opponentSum;
    return newRounds
  }

  addCardToPlayerBoard(card) {
    const { playerBoard0, playerBoard1, playerBoard2 } = this.props;

    if(card.row === 0) {
      this.props.savePlayerBoard0(this.addToArray(card, playerBoard0));
    }
    
    if(card.row === 1) {
      this.props.savePlayerBoard1(this.addToArray(card, playerBoard1));
    }

    if(card.row === 2) {
      this.props.savePlayerBoard2(this.addToArray(card, playerBoard2));
    }
  }

  playCard(selectedCard) {
    const { playerHand } = this.props;
    // TODO: Apply card ability

    this.props.savePlayerHand(this.deleteFromArray(selectedCard, playerHand));
    
    if (selectedCard.type === "normal") {
      this.addCardToPlayerBoard(selectedCard);
    }

    // if (selectedCard.type === "leader") {
      // this.setState({playerHand: this.deleteFromArray(selectedCard, playerHand),})
      // newState.player[currentPlayer].rowEnds[selectedCard.row] = selectedCard;
    // }

    if (selectedCard.type === "special") {
      // Either add effect to a row OR clear all rows   
      let newSpecials = this.props.specials.slice();

      if(selectedCard.row !== -1) {
        newSpecials[selectedCard.row] = true;
      }
      else {
        newSpecials = [false, false, false];
      }
      this.props.saveStatsSpecials(newSpecials);
    }

    // Switch to other player unless other player has passed
    if (!this.props.opponentPassed) {
      this.props.saveStatsCurrentPlayer(this.props.opponentId);
    }
  }  

  pass() {
    this.props.savePlayerPassed(true);

    if (!this.props.opponentPassed) {
      this.props.saveStatsCurrentPlayer(this.props.opponentId);
    }
  }

  nextRound() {
    this.props.saveStats(
    {
      "rounds": this.props.rounds,
      "round": this.props.round + 1,
      "roundEnded": false,
      "currentPlayer": this.props.currentPlayer,
      "specials": [false, false, false],
      "hasTwoPlayers": true,
      "drawCards": false,
    });
    this.props.savePlayerBoard0(null);
    this.props.savePlayerBoard1(null);
    this.props.savePlayerBoard2(null);
    this.props.saveOpponentBoard0(null);
    this.props.saveOpponentBoard1(null);
    this.props.saveOpponentBoard2(null);
    // TODO: Move cards to the discard pile
    // TODO: Fix Mainly for Jarvis double passing
    this.props.savePlayerPassed(false);
    this.props.saveOpponentPassed(false);
  }

  // Internal functions:

  calculateScore(board0, board1, board2, specials) {

    let total = [0, 0, 0];

    [board0, board1, board2].map(function (row, i) {
      row.map(function (card) {
        if ( specials[i] ) {
          total[i] += 1;
        }
        else {
          total[i] += card.value;
        }          
        return true
      });
      return true
    });

    return total
  }

  deleteFromArray(cardObj, array) {
    if(typeof array === "undefined") {
      return []
    }
    const arrayClone = array.slice(0);
    return arrayClone.filter(function (el) { return el.id !== cardObj.id; });
  }

  addToArray(cardObj, array) {
    if(typeof array === "undefined") {
      return [cardObj]
    }
    const arrayClone2 = array.slice(0);
    arrayClone2.push(cardObj);
    return arrayClone2;
  }

  render() {
    return (
      <div className="game-logic">
        <Game
          // Functions
          playCard={this.playCard}
          pass={this.pass}
          nextRound={this.nextRound}
          
          // Props
          round={this.props.round}
          rounds={this.props.rounds}
          currentRound={this.props.currentRound}
          roundEnded={this.props.roundEnded}
          currentPlayer={this.props.currentPlayer}
          specials={this.props.specials}

          playerId={this.props.playerId}
          playerHand={this.props.playerHand}
          playerBoard0={this.props.playerBoard0}
          playerBoard1={this.props.playerBoard1}
          playerBoard2={this.props.playerBoard2}
          playerRowEnds={this.props.playerRowEnds}
          playerDiscarded={this.props.playerDiscarded}
          playerLeader={this.props.playerLeader}
          playerDeck={this.props.playerDeck}
          playerPassed={this.props.playerPassed}
          playerCharacterName={this.props.playerCharacterName}        

          opponentId={this.props.opponentId}
          opponentHand={this.props.opponentHand}
          opponentBoard0={this.props.opponentBoard0}
          opponentBoard1={this.props.opponentBoard1}
          opponentBoard2={this.props.opponentBoard2}
          opponentRowEnds={this.props.opponentRowEnds}
          opponentLeader={this.props.opponentLeader}
          opponentDeck={this.props.opponentDeck}
          opponentPassed={this.props.opponentPassed}
          opponentCharacterName={this.props.opponentCharacterName}

          // From state
          playerTotal={this.state.playerTotal}
          opponentTotal={this.state.opponentTotal}
        />

        { this.props.singlePlayer &&
          <Jarvis
            saveJarvisPassed={this.props.saveOpponentPassed}
            saveJarvisHand={this.props.saveOpponentHand}
            saveJarvisBoard0={this.props.saveOpponentBoard0}
            saveJarvisBoard1={this.props.saveOpponentBoard1}
            saveJarvisBoard2={this.props.saveOpponentBoard2}
            saveStatsSpecials={this.props.saveStatsSpecials}
            saveStatsCurrentPlayer={this.props.saveStatsCurrentPlayer}

            currentPlayer={this.props.currentPlayer}
            specials={this.props.specials}
            roundEnded={this.props.roundEnded}

            playerPassed={this.props.playerPassed}

            jarvisHand={this.props.opponentHand}
            jarvisBoard0={this.props.opponentBoard0}
            jarvisBoard1={this.props.opponentBoard1}
            jarvisBoard2={this.props.opponentBoard2}
          />
        }
      </div>
    )
  }
}

GameLogic.propTypes = {
  // From App
  gameId: React.PropTypes.string.isRequired,
  playerId: React.PropTypes.number.isRequired,
  opponentId: React.PropTypes.number.isRequired,
  // From firebase
  // saveStats: React.PropTypes.func.isRequired,
  // savePlayer: React.PropTypes.func.isRequired,
  // saveOpponent: React.PropTypes.func.isRequired,

  round: React.PropTypes.number.isRequired,
  rounds: React.PropTypes.array.isRequired,
  roundEnded: React.PropTypes.bool.isRequired,
  currentPlayer: React.PropTypes.number.isRequired,
  specials: React.PropTypes.array.isRequired,

  playerHand: React.PropTypes.array,
  playerBoard0: React.PropTypes.array,
  playerBoard1: React.PropTypes.array,
  playerBoard2: React.PropTypes.array,
  playerRowEnds: React.PropTypes.array,
  playerDiscarded: React.PropTypes.array,
  playerLeader: React.PropTypes.object,
  playerDeck: React.PropTypes.array.isRequired,
  playerPassed: React.PropTypes.bool.isRequired,

  opponentBoard0: React.PropTypes.array,
  opponentBoard1: React.PropTypes.array,
  opponentBoard2: React.PropTypes.array,
  opponentRowEnds: React.PropTypes.array,
  opponentPassed: React.PropTypes.bool.isRequired,
}

GameLogic.defaultProps = {
  playerDiscarded: [],

  playerHand: [],
  playerBoard0: [],
  playerBoard1: [],
  playerBoard2: [],
  playerRowEnds: [null, null, null],
  playerLeader: {},
  playerPassed: false,  
  
  opponentHand: [],
  opponentBoard0: [],
  opponentBoard1: [],
  opponentBoard2: [],
  opponentRowEnds: [null, null, null],
  opponentPassed: false,
}

export default GameLogic
