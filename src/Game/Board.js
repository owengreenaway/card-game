import React, { Component } from "react";
import Card from "./Card";

class Board extends Component {
  //   constructor(props) {
  //     super(props);
  //     this.state = this.props;    
  //     this.clickHandler = this.clickHandler.bind(this);
  //   }

  render() {
    const { board0, board1, board2 , specials, player, _selectCard, reverse } = this.props;
    return (
      <div className="Board col-xs-9">
        { reverse ?
          <div className="row">
            <div className={(specials[2]) ? "col-xs-12 cardHeight border Board__row Board__row--disabled" : "col-xs-12 cardHeight border Board__row Board__row--seige"}>
              {(board2.map(function (card, i) {
                return <Card key={i} details={card} __selectCard={_selectCard} player={player} playableCards={false} />
              }))}
            </div>
            <div className={(specials[1]) ? "col-xs-12 cardHeight border Board__row Board__row--disabled" : "col-xs-12 cardHeight border Board__row Board__row--archers"}>
              {(board1.map(function (card, i) {
                return <Card key={i} details={card} __selectCard={_selectCard} player={player} playableCards={false} />
              }))}
            </div>
            <div className={(specials[0]) ? "col-xs-12 cardHeight border Board__row Board__row--disabled" : "col-xs-12 cardHeight border Board__row Board__row--infantry"}>
              {(board0.map(function (card, i) {
                return <Card key={i} details={card} __selectCard={_selectCard} player={player} playableCards={false} />
              }))}
            </div>
          </div>
        :
          <div className="row">
            <div className={(specials[0]) ? "col-xs-12 cardHeight border Board__row Board__row--disabled" : "col-xs-12 cardHeight border Board__row Board__row--infantry"}>
              {(board0.map(function (card, i) {
                return <Card key={i} details={card} __selectCard={_selectCard} player={player} playableCards={false} />
              }))}
            </div>
            <div className={(specials[1]) ? "col-xs-12 cardHeight border Board__row Board__row--disabled" : "col-xs-12 cardHeight border Board__row Board__row--archers"}>
              {(board1.map(function (card, i) {
                return <Card key={i} details={card} __selectCard={_selectCard} player={player} playableCards={false} />
              }))}
            </div>
            <div className={(specials[2]) ? "col-xs-12 cardHeight border Board__row Board__row--disabled" : "col-xs-12 cardHeight border Board__row Board__row--seige"}>
              {(board2.map(function (card, i) {
                return <Card key={i} details={card} __selectCard={_selectCard} player={player} playableCards={false} />
              }))}
            </div>
          </div>
        }
      </div>
    )
  }
}

Board.propTypes = {
  board: React.PropTypes.array,
  specials: React.PropTypes.array,
};

Board.defaultProps = {
  board: [[],[],[]],
  specials: [[],[],[]],
};

export default Board