import React, { Component } from 'react';
import { connect } from 'react-firebase';
import GameLogic from "./GameLogic";

class GameDefaults extends Component {
  render() {
    const firebaseNotFinishedLoading =
      typeof this.props.round === "undefined"
      || typeof this.props.rounds === "undefined"
      || typeof this.props.roundEnded === "undefined"
      || typeof this.props.currentPlayer === "undefined"
      || typeof this.props.specials === "undefined"
      || typeof this.props.playerDeck === "undefined"
      || typeof this.props.playerPassed === "undefined"
      || typeof this.props.opponentPassed === "undefined"
      ;
    // console.log(loadGame);
    // console.log(typeof this.props.round);
    return (
      <div className="GameDefaults">
        { !firebaseNotFinishedLoading &&
          <GameLogic
            // From App
            gameId={this.props.gameId}
            playerId={this.props.playerId}
            opponentId={this.props.opponentId}
            singlePlayer={this.props.singlePlayer}
            // From firebase
            // firebase returns null. DefaultProps are only applied for undefined.
            round={this.props.round}
            rounds={this.props.rounds}
            currentRound={this.props.currentRound}
            roundEnded={this.props.roundEnded}
            currentPlayer={this.props.currentPlayer}
            specials={this.props.specials}

            playerHand={this.props.playerHand || undefined}
            playerBoard0={this.props.playerBoard0 || undefined}
            playerBoard1={this.props.playerBoard1 || undefined}
            playerBoard2={this.props.playerBoard2 || undefined}
            playerRowEnds={this.props.playerRowEnds || undefined}
            playerDiscarded={this.props.playerDiscarded || undefined}
            playerLeader={this.props.playerLeader || undefined}
            playerDeck={this.props.playerDeck || undefined}
            playerPassed={this.props.playerPassed || undefined}
            playerCharacterName={this.props.playerCharacterName || undefined}

            opponentBoard0={this.props.opponentBoard0 || undefined}
            opponentBoard1={this.props.opponentBoard1 || undefined}
            opponentBoard2={this.props.opponentBoard2 || undefined}
            opponentRowEnds={this.props.opponentRowEnds || undefined}
            opponentPassed={this.props.opponentPassed || undefined}
            opponentHand={this.props.opponentHand || undefined}
            opponentCharacterName={this.props.opponentCharacterName || undefined}


            // Firebase methods
            savePlayerPassed={this.props.savePlayerPassed}
            saveOpponentPassed={this.props.saveOpponentPassed}
            savePlayerBoard0={this.props.savePlayerBoard0}
            savePlayerBoard1={this.props.savePlayerBoard1}
            savePlayerBoard2={this.props.savePlayerBoard2}
            savePlayerHand={this.props.savePlayerHand}
            saveStatsRounds={this.props.saveStatsRounds}
            saveStatsRoundEnded={this.props.saveStatsRoundEnded}
            saveStatsSpecials={this.props.saveStatsSpecials}
            saveStatsCurrentPlayer={this.props.saveStatsCurrentPlayer}
            saveStats={this.props.saveStats}
            saveOpponentBoard0={this.props.saveOpponentBoard0}
            saveOpponentBoard1={this.props.saveOpponentBoard1}
            saveOpponentBoard2={this.props.saveOpponentBoard2}
            saveOpponentHand={this.props.saveOpponentHand}
          />
        }
      </div >
    )
  }
}

GameDefaults.propTypes = {
  // From App
  gameId: React.PropTypes.string,
  playerId: React.PropTypes.number,
  opponentId: React.PropTypes.number,

  // From firebase
  round: React.PropTypes.number,
  rounds: React.PropTypes.array,
  currentRound: React.PropTypes.number,
  roundEnded: React.PropTypes.bool,
  currentPlayer: React.PropTypes.number,
  specials: React.PropTypes.array,

  playerHand: React.PropTypes.array,
  playerBoard0: React.PropTypes.array,
  playerBoard1: React.PropTypes.array,
  playerBoard2: React.PropTypes.array,
  playerRowEnds: React.PropTypes.array,
  playerDiscarded: React.PropTypes.array,
  playerLeader: React.PropTypes.object,
  playerDeck: React.PropTypes.array,
  playerPassed: React.PropTypes.bool,

  opponentBoard0: React.PropTypes.array,
  opponentBoard1: React.PropTypes.array,
  opponentBoard2: React.PropTypes.array,
  opponentRowEnds: React.PropTypes.array,
  opponentPassed: React.PropTypes.bool,

  // Methods
  savePlayerPassed: React.PropTypes.func,
  saveOpponentPassed: React.PropTypes.func,
  savePlayerBoard0: React.PropTypes.func,
  savePlayerBoard1: React.PropTypes.func,
  savePlayerBoard2: React.PropTypes.func,
  saveStatsRounds: React.PropTypes.func,
  saveStatsRoundEnded: React.PropTypes.func,
}

export default connect((props, ref) => ({
  round: "/games/"+props.gameId+"/gameStats/round",
  rounds: "/games/"+props.gameId+"/gameStats/rounds",
  roundEnded: "/games/"+props.gameId+"/gameStats/roundEnded",
  currentPlayer: "/games/"+props.gameId+"/gameStats/currentPlayer",
  specials: "/games/"+props.gameId+"/gameStats/specials",

  playerHand: "/games/"+props.gameId+"/player/"+props.playerId+"/hand",
  playerBoard0: "/games/"+props.gameId+"/player/"+props.playerId+"/board0",
  playerBoard1: "/games/"+props.gameId+"/player/"+props.playerId+"/board1",
  playerBoard2: "/games/"+props.gameId+"/player/"+props.playerId+"/board2",
  playerRowEnds: "/games/"+props.gameId+"/player/"+props.playerId+"/rowEnds",
  playerDiscarded: "/games/"+props.gameId+"/player/"+props.playerId+"/discarded",
  playerLeader: "/games/"+props.gameId+"/player/"+props.playerId+"/leader",
  playerDeck: "/games/"+props.gameId+"/player/"+props.playerId+"/deck",
  playerPassed: "/games/"+props.gameId+"/player/"+props.playerId+"/passed",
  playerCharacterName: "/games/"+props.gameId+"/player/"+props.playerId+"/character",

  opponentCharacterName: "/games/"+props.gameId+"/player/"+props.opponentId+"/character",
  opponentHand: "/games/"+props.gameId+"/player/"+props.opponentId+"/hand",
  opponentBoard0: "/games/"+props.gameId+"/player/"+props.opponentId+"/board0",
  opponentBoard1: "/games/"+props.gameId+"/player/"+props.opponentId+"/board1",
  opponentBoard2: "/games/"+props.gameId+"/player/"+props.opponentId+"/board2",
  opponentRowEnds: "/games/"+props.gameId+"/player/"+props.opponentId+"/rowEnds",
  opponentPassed: "/games/"+props.gameId+"/player/"+props.opponentId+"/passed",

  // firebaseSet: (url,value) => ref("/games/"+props.gameId+"/"+url).set(value),
  savePlayerPassed: (value) => ref("/games/"+props.gameId+"/player/"+props.playerId+"/passed").set(value),
  saveOpponentPassed: (value) => ref("/games/"+props.gameId+"/player/"+props.opponentId+"/passed").set(value),
  savePlayerHand: (value) => ref("/games/"+props.gameId+"/player/"+props.playerId+"/hand").set(value),
  savePlayerBoard0: (value) => ref("/games/"+props.gameId+"/player/"+props.playerId+"/board0").set(value),
  savePlayerBoard1: (value) => ref("/games/"+props.gameId+"/player/"+props.playerId+"/board1").set(value),
  savePlayerBoard2: (value) => ref("/games/"+props.gameId+"/player/"+props.playerId+"/board2").set(value),
  saveStats: (value) => ref("/games/"+props.gameId+"/gameStats/").set(value),
  saveStatsRounds: (value) => ref("/games/"+props.gameId+"/gameStats/rounds").set(value),
  saveStatsRoundEnded: (value) => ref("/games/"+props.gameId+"/gameStats/roundEnded").set(value),
  saveStatsSpecials: (value) => ref("/games/"+props.gameId+"/gameStats/specials").set(value),
  saveStatsCurrentPlayer: (value) => ref("/games/"+props.gameId+"/gameStats/currentPlayer").set(value),
  saveOpponentBoard0: (value) => ref("/games/"+props.gameId+"/player/"+props.opponentId+"/board0").set(value),
  saveOpponentBoard1: (value) => ref("/games/"+props.gameId+"/player/"+props.opponentId+"/board1").set(value),
  saveOpponentBoard2: (value) => ref("/games/"+props.gameId+"/player/"+props.opponentId+"/board2").set(value),
  saveOpponentHand: (value) => ref("/games/"+props.gameId+"/player/"+props.opponentId+"/hand").set(value),
}))(GameDefaults)