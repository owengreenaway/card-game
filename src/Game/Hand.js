import React, { Component } from "react";
import Card from "./Card";

class Hand extends Component {
//   constructor(props) {
//     super(props);
//     this.state = this.props;    
//     this.clickHandler = this.clickHandler.bind(this);
//   }

  render() {
    const { cards, _selectCard } = this.props;

    return (
      <div className="Hand col-xs-12 text-center cardHeight">
        {(
          cards && cards.map(function(card, i){
            return <Card key={i} details={card} __selectCard={_selectCard} playableCards={true}/>
          })
        )}
      </div>
    )
  }
}

Hand.propTypes = {
  cards: React.PropTypes.array.isRequired,
  _selectCard: React.PropTypes.func.isRequired,
};

export default Hand