import React, { Component } from "react";

class CardInfo extends Component {
  render() {
    const { isSelectedPlayable, selectedCard } = this.props;
    return (
      selectedCard &&
      (
        
        <div className="col-xs-12 CardInfo">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title">{selectedCard.name}</h3>
                </div>
                <div className="panel-body">
                  <div className="CardInfo__imageCropper">
                    <img className="CardInfo__image" src={"images/cards/"+selectedCard.image+ "-gwent-card.jpg"} alt={selectedCard.name} />
                  </div>
                
                  { isSelectedPlayable &&
                    <div className="CardInfo__button">
                      <button className="btn btn-default btn-block" onClick={this.props._playCard}>Play Card</button>
                    </div>
                  }
                </div>
              </div>

          
          {/*<div className="CardInfo__name">
            <div>{name}</div>
          </div>
          <div className="CardInfo__value">
            <div><strong>{value}</strong></div>
          </div>
          <div className="CardInfo__info">
            <div>{faction}</div>
            <div>Row: {row}</div>
            <div>Id: {id}</div>
            <div>{type}</div>
            { ability && <div>abilityDescription</div> }
          </div>*/}

          
        </div>
      )
    )
  }
}

CardInfo.PropTypes = {
  _playCard: React.PropTypes.func.isRequired,
  selectedCard: React.PropTypes.object,
};

CardInfo.defaultProps = {
  selectedCard: null
};

export default CardInfo