import React, { Component } from "react";
import firebase from 'firebase';
import GameDefaults from "./Game/GameDefaults.js";
import Character from "./Character/Character.js";
import InitiateGame from "./InitiateGame/InitiateGame.js";
import "./App.css";

// Initialise only once in the whole app
firebase.initializeApp({
  databaseURL: 'https://card-game-fc259.firebaseio.com'
})

class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      characterId: null,
      gameId: null,
      opponentId: null,   
      playerId: null,
      singlePlayer: false,

      // characterId: "-KiyMcVYemNtptqudXLw",
      // gameId: "-Kj2HZgWVCaKmpJylmG1",
      // opponentId: 1,   
      // playerId: 0,
      // singlePlayer: true,
    }
  }

  render() {
    const { opponentId, playerId, gameId, characterId, singlePlayer} = this.state;

    return (
      <div className="app">
        <div className="container">
          {/*
          <div className="row">
            <div className="col-sm-6 col-sm-offset-3">
              <div className="panel panel-default">
                <div className="panel-heading">
                  <h3 className="panel-title">Panel title</h3>
                </div>
                <div className="panel-body">
                  Panel content
                </div>
              </div>
            </div>
          </div>
          */}

          {!characterId &&
            <Character saveCharacterIdtoApp={(id)=>{ this.setState( {characterId:id} ) } } />
          }

          { characterId && !gameId && 
            <InitiateGame
              characterId={this.state.characterId}
              saveToApp={(obj)=>{ this.setState( obj ) } }
              singlePlayer={singlePlayer}
            />
          }

          { gameId &&
            <GameDefaults
              gameId={gameId}
              playerId={playerId}
              opponentId={opponentId}
              singlePlayer={singlePlayer}
            />
          }
        </div>
      </div>
    );
  }
}

export default App