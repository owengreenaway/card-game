## Resources
- https://firebase.google.com/

## Deploy
~~~
npm run build
firebase deploy --only hosting
~~~

## Todo
- move is playable to <Hand /> so board e.t.c doesn't need player
- create a utils file of unit testable functions

## Changelog
- 0.0.1 Initial release
- 0.0.2 React props were tightly coupled to the firebase database structure. Complete refactoring to decouple.
- 0.0.3 Added Jarvis (computer player)
- 0.1.0 Added ability to have characters. Each component only connects to the part of the database it requires, not the whole thing.